<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `doctors`";
// var_dump($query);
include 'header.php'; 
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid margin-top">
            <a href="create-doctor.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Doctor</a>  
            <div class="table-responsive">     
            <table class="table table-hover margin-top">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Specialization</th>
                  <th>Department</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Office no.</th>
                  <th>Biodata</th>
                  <th>Picture</th>
                  <th>Created At</th>
                  <th>Modified At</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                   <!-- <tr>
                    <td>1</td>
                    <td>Jalal</td>
                    <td>NEPHROLOGY</td>
                    <td>Health Care</td>
                    <td>10-5-17</td>
                    <td>10am to 11am</td>
                    <td>220</td>
                    <td>test</td>
                    <td>jalal.jpg</td>
                    <td>5-15-17</td>
                    <td>5-15-17</td>
                    <td>
                      <a href="#" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="#" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="#" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr>  -->
             
                  <?php  
                  foreach($db->query($query) as $doctors) { ?>
                  <tr>
                    <td><?php echo $doctors['id'];?></td>
                    <td><?php echo $doctors['name'];?></td>
                    <td><?php echo $doctors['specialist'];?></td>
                    <td><?php echo $doctors['department'];?></td>
                    <td><?php echo $doctors['available_date'];?></td>
                    <td><?php echo $doctors['time'];?></td>
                    <td><?php echo $doctors['office'];?></td>
                    <td><?php echo $doctors['biodata'];?></td>
                    <td><img width="80px" src="images/<?php echo $doctors["picture"]; ?>"/> </td>
                    <td><?php echo date("d/m/Y", strtotime($doctors['created_at']));?></td>
                    <td><?php echo date("d/m/Y", strtotime($doctors['modified_at']));?></td>
                    <td>
                      <a href="view-single-doctor.php?id=<?php echo $doctors['id'];?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="edit-doctor.php?id=<?php echo $doctors['id'];?>" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="delete-doctor.php?id=<?php echo $doctors['id'];?>" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr>
                <?php }
                ?>
                
              </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>