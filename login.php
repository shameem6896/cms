<?php include 'header.php';  ?>

    <body class="panel-access">

        <div id="layout">
             <!--Login-->
                <div class="login">
                    <div class="container">
                        <div class="login-form">
                            <div class="data-form">
                                <span class="back-to-homepage">
                                    <a class="btn btn-green btn-xsmall" href="index.php"><i class="fa fa-home"></i> Back to Homepage</a>
                                </span>
                                <!--Logo-->
                                <a href="index.php" class="logo reg-logo"><img src="images/login-logo.png" alt="logo" class="img-responsive"></a>
                                <!--Logo-->

                                <!--Form-->
                                <form class="form-login">
                                    

                                    <div class="icon-data">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <input type="email" placeholder="Enter Email">

                                    <div class="icon-data">
                                        <i class="fa fa-key"></i>
                                    </div>
                                    <input type="password" placeholder="Password">

                                    <a href="appointments-reserved-empty.php" class="btn btn-default" role="button">Login</a>
                                    <!--<button class="btn btn-default" type="submit">Login</button>-->
                                </form>
                                <!--Form-->

                                <a href="register.php" class="btn btn-red register" role="button">Create a new account</a>
                                <span class="help">
                                    <a href="forgot-pass.php">Forgot Password?</a>
                                    <a href="help.php" class="help-link">Help?</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

        <?php include 'footer.php';  ?>
        