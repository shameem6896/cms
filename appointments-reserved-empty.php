<?php include 'header.php';  ?>
<body>
<div id="layout">
<header>
	<div class="container">
		<div class="row">
		 
		<div class="logo inner-logo">
			<a href="index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
		</div>
		 
		 
		<div class="tools-top">
		 
			<div class="avatar-profile">
				<div class="user-edit">
					<h4>Jalal Ahamad</h4>
					<a href="my-account.php">edit profile</a>
				</div>
				<div class="avatar-image">
					<img src="images/avatar-profile.jpg" alt="avatar profile" class="img-responsive">
					<a href="appointments-reserved.php" title="2 Notifications Pending">
					<span class="notifications" style="display: inline;">2</span>
					</a>
				</div>
			</div>
			 
			<ul class="tools-help">
				<li><a href="help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
				<li><a href="login.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
			</ul>
		</div>
		 
		</div>
	</div>
</header>
 
<nav>
	<div class="container">
		<h4 class="navbar-brand">menu</h4>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<i class="fa fa-bars" aria-hidden="true"></i>
			</button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li> <a href="index.php">Home</a> </li>
				<li class="active"> <a href="appointments-reserved-empty.php">appointments reserved</a> </li>
				<li> <a href="booked-calendar.php">book an appointment</a> </li>
				<li> <a href="examinations.php">Result Examinations</a> </li>
				<li> <a href="my-account.php">my account</a> </li>
			</ul>
			
			</div>
		</div>
	</div>
</nav>
 
<section class="container">
	<div class="main-container">
		<div class="row">
		<div class="listed">
			<div class="row">
			 
			<div class="col-lg-12">
				<h3>You don't have any appointments yet.</h3>
				<hr>
			</div>
			 
			</div>
			<div class="row">
				<div class="load-more">
				<a class="btn btn-green btn-small" href="booked-calendar.php"> Book Now</a>
				</div>
			</div>
		</div>
		 
		<aside>
		<div class="elements-aside">
			<ul>
				<li class="color-2">
				<i class="fa fa-hourglass-half" aria-hidden="true"></i>
				<h4>Working Time</h4>
				<p>Monday to Friday <span> 09:00am to 05:00pm</span></p>
				<p>Weekends <span> 09:00am to 12:00pm</span></p>
				</li>
				<li class="color-3">
				<i class="fa fa-info" aria-hidden="true"></i>
				<h4>Doubts?</h4>
				<p>Office 8/2A, Katasur, Mohammadpur <span>Dhaka-1207</span></p>
				</li>
			</ul>
		</div>
		</aside>
		 
		</div>
	</div>
</section>
<?php include 'footer.php';  ?>
