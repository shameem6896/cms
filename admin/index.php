<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Login</title>
    <link href="css/login.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <body class="panel-access">
        <div id="layout">
             <!--Login-->
            <div class="login">
                <div class="container">
                    <div class="login-form">
                        <div class="data-form">
                            <a href="index.php" class="logo reg-logo"><img src="images/admin-logo.png" alt="logo" class="img-responsive"></a>

                          <?php /*  <?php if(!isset($_SESSION['name'])){

                                if(isset($_GET['error_msg'])){?>
                                <div class="alert alert-warning"><?php echo $_GET['error_msg'];?></div>

                            <?php } ?>

*/ ?>
                            <?php
                            if( isset($_SESSION['error_msg']) && is_array($_SESSION['error_msg']) && count($_SESSION['error_msg']) >0 ) {
                            echo '<div class="alert alert-warning">';
                                foreach($_SESSION['error_msg'] as $msg) {
                                echo $msg; 
                            }
                            echo '</div>';
                            unset($_SESSION['error_msg']);
                            }
                            ?>

                            <form class="form-login" role="form" method="post" action="check_login.php">
                                <div class="icon-data">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" placeholder="User Name" name="username" required="required">
                                <div class="icon-data">
                                    <i class="fa fa-key"></i>
                                </div>
                                <input type="password" placeholder="Password" name="password" required="required">
                                <button type="submit" class="btn btn-default" role="button">Login</button>
                            </form>

                         <?php /*   <?php } else {?>  
                                <h3 class="welcomeUser">Welcome <?php echo $_SESSION['name'];?></h3>
                            <?php } ?>  
                            */ ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
        