<?php include 'header.php';  ?>

    <body>
        <div id="layout">
            <header>
                <div class="menu">
                    <div class="container">
                        <div class="row">
                            <div class="logo inner-logo">
                               <a href="index.php"><img src="images/logo-white.png" alt="logo main" class="img-responsive"></a>
                            </div>
                            <div class="meet-social social-info">
                                <span><a href="#"><i class="fa fa-facebook-square"></i></a></span>
                                <span><a href="#"><i class="fa fa-twitter-square"></i></a></span>
                                <span><a href="#"><i class="fa fa-google-plus-square"></i></a></span>
                                <span><a href="#"><i class="fa fa-linkedin-square"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <!--Menu-->
            <nav>
                <div class="container">
                    <h4 class="navbar-brand">menu</h4>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav">
                            <li> <a href="index.php">Home</a> </li>
                            <li class="active"> <a href="meet-doctors.php">Meet Doctors</a> </li>
                            <li> <a href="booked-calendar.php">book an appointment</a> </li>
                            <li> <a href="examinations.php">Result Examinations</a> </li>
                            <li> <a href="my-account.php">my account</a> </li>
                            <li> <a href="login.php">Login</a> </li>
                            <li> <a href="register.php">Register</a> </li>
                        </ul>
                     
                    </div>
                </div>
            </nav>
            <!--Menu-->

            <div class="container">
                <div class="main-container">
                    <div class="row">
                        <div class="department-title">
                            <h3>Surgery Department</h3>
                            <hr>
                        </div>
                        <!--Item-->
                        <div class="meet-doctors">
                            <div class="about-doctor">
                                <div class="personal-data">
                                    <img src="images/doctor11.jpg" alt="doctor" class="img-responsive">
                                    <h4>Prof. (Dr.) Asia Khanam</h4>
                                    <span>Nephrology</span>
                                    <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                                    <p class="social-media">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-globe"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-envelope"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--Item-->

                        <!--Item-->
                        <div class="meet-doctors">
                            <div class="about-doctor">
                                <div class="personal-data">
                                    <img src="images/doctor17.jpg" alt="doctor" class="img-responsive">
                                    <h4>Prof. Dr. Abu Nasar Rizvi</h4>
                                    <span>Nephrology</span>
                                    <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                                    <p class="social-media">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-globe"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-envelope"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--Item-->

                        <!--Item-->
                        <div class="meet-doctors">
                            <div class="about-doctor">
                                <div class="personal-data">
                                    <img src="images/doctor13.jpg" alt="doctor" class="img-responsive">
                                    <h4>Dr. Amjad Hossain</h4>
                                    <span>Orthopedic & Trauma</span>
                                    <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                                    <p class="social-media">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-globe"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-envelope"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--Item-->

                        <div class="department-title">
                            <h3>Health Care Deparment</h3>
                            <hr>
                        </div>
                        <!--Item-->
                        <div class="meet-doctors">
                            <div class="about-doctor">
                                <div class="personal-data">
                                    <img src="images/doctor16.jpg" alt="doctor" class="img-responsive">
                                    <h4>DR. MD. MOFIZ UDDIN</h4>
                                    <span>ENT ( Ear,Nose &Throat)</span>
                                    <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                                    <p class="social-media">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-globe"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-envelope"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--Item-->

                        <!--Item-->
                        <div class="meet-doctors">
                            <div class="about-doctor">
                                <div class="personal-data">
                                    <img src="images/doctor15.jpg" alt="doctor" class="img-responsive">
                                    <h4>Dr. Md. Lokman Hossain</h4>
                                    <span>Cardiac Surgery</span>
                                    <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                                    <p class="social-media">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-globe"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-envelope"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--Item-->
                    </div>
                </div>
            </div>

    <?php include 'footer.php';  ?>
