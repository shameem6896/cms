<?php include 'header.php';  ?>

    <body>

        <div id="layout">

            <header>
                <div class="container">
                    <div class="row">
                        <!--Logo-->
                        <div class="logo inner-logo">
                            <a href="index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
                        </div>
                        <!--Logo-->

                        <!--Header tools-->
                        <div class="tools-top">
                            <!--Avatar-->
                            <div class="avatar-profile">
                                <div class="user-edit">
                                    <h4>Jalal Ahamad</h4>
                                    <a href="my-account.php">edit profile</a>
                                </div>
                                <div class="avatar-image">
                                    <img src="images/avatar-profile.jpg" alt="avatar profile" class="img-responsive">
                                    <a href="appointments-reserved.php" title="2 Notifications Pending">
                                        <span class="notifications" style="display: inline;">2</span>
                                    </a>
                                </div>
                            </div>
                            <!--Avatar-->

                            <ul class="tools-help">
                                <li><a href="help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
                                <li><a href="login.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
                            </ul>
                        </div>
                        <!--Header tools-->
                    </div>
                </div>
            </header>

            <!--Menu-->
            <nav>
                <div class="container">
                    <h4 class="navbar-brand">menu</h4>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav">
                            <li> <a href="index.php">Home</a> </li>
                            <li> <a href="appointments-reserved.php">appointments reserved</a> </li>
                            <li> <a href="booked-calendar.php">book an appointment</a> </li>
                            <li> <a href="examinations.php">Result Examinations</a> </li>
                            <li class="active"> <a href="my-account.php">my account</a> </li>
                        </ul>
                       
                    </div>
                </div>
            </nav>
            <!--Menu-->

            <div class="container">
                <div class="main-container">
                    <!--Form-->
                    <div class="register-form edit-account">

                        <div class="form-login">
                            <form>
                                <!--Personal Information-->
                                <div class="row">

                                    <h3>Personal Information</h3>
                                    <div class="datapos">

                                        <!--select a document-->
                                        <div class="icon-data">
                                            <i class="fa fa-address-card"></i>
                                        </div>
                                        <select disabled="" class="disable">
                                            <option selected="">Id Number</option>
                                        </select>
                                        <!--select a document-->

                                        <!--name-->
                                        <div class="icon-data">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" placeholder="Jalal">
                                        <!--name-->

                                        <!--phone-->
                                        <div class="icon-data">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="number" placeholder="01813-323479">
                                        <!--phone-->
                                        <!--gender-->
                                        <label>
                                            <input type="radio" value="female"> <span>Female</span>
                                        </label>
                                        <label>
                                            <input type="radio" checked="" value="male"> Male
                                        </label>
                                        <!--gender-->
                                    </div>

                                    <div class="datapos">

                                         <!--Alternative phone-->
                                        <div class="icon-data">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="email" placeholder="jalal@gmail.com" readonly="">
                                        <!--Alternative phone-->
                                        <!--Last name-->
                                        <div class="icon-data">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" placeholder="Ahamad">
                                        <!--Last name-->
                                       
                                        <div class="icon-data">
                                            <i class="fa fa-user-circle"></i>
                                        </div>
                                        <input type="password" placeholder="Password">
                                        <!--Alternative phone-->

                                         <!--Avatar Profile-->
                                        <div class="avatar-profile">
                                            <label>Image Profile</label>
                                            <input type="file" accept="image/*">
                                        </div>
                                    </div>

                                </div>
                                <!--Personal Information-->
                                <button class="btn btn-default">save changes</button>

                                <span class="help">
                                    <a href="my-account.php#" class="help-link">Help?</a>
                                </span>
                            </form>
                        </div>
                    </div>
                    <!--Form-->
                </div>
            </div>

    <?php include 'footer.php';  ?>