<?php include 'header.php';  ?>

    <body>

        <div id="layout">
            <header>
                <div class="menu">
                    <div class="container">
                        <div class="row">
                            <div class="logo inner-logo">
                               <a href="index.php"><img src="images/logo-white.png" alt="logo main" class="img-responsive"></a>
                            </div>
                            <div class="meet-social">
                                <span><a href="#"><i class="fa fa-facebook-square"></i></a></span>
                                <span><a href="#"><i class="fa fa-twitter-square"></i></a></span>
                                <span><a href="#"><i class="fa fa-google-plus-square"></i></a></span>
                                <span><a href="#"><i class="fa fa-linkedin-square"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <!--Menu-->
            <nav>
                <div class="container">
                    <h4 class="navbar-brand">menu</h4>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav">
                            <li> <a href="index.php">Home</a> </li>
                            <li> <a href="blog.php">Blog</a> </li>
                            <li> <a href="login.php">Login</a> </li>
                            <li> <a href="my-account.php">my account</a> </li>
                            <li> <a href="help.php">Help </a></li>
                            <li> <a href="meet-doctors.php">Find Doctors</a> </li>
                            <li class="active"> <a href="404-page.php">Error - 404</a> </li>
                        </ul>
                       
                    </div>
                </div>
            </nav>
            <!--Menu-->

            <section class="container">
                <div class="main-container">
                    <div class="row">
                        <div class="error-404">
                            <h1>Oops! That page can’t be found.</h1>
                            <hr>
                            <p>It looks like nothing was found at this location. Maybe try one of the links on our menu</p>
                        </div>
                    </div>
                </div>
            </section>

        <?php include 'footer.php';  ?>
