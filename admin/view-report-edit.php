<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `reports` WHERE id =".$_GET['id'];
// var_dump($query);
include 'header.php'; 
foreach($db->query($query) as $row) {
  $reports = $row;
}
?>

<?php include 'header.php'; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'navigation.php'; ?>

        <div id="page-wrapper">
            <div class="container-fluid" style="margin-top: 20px;">
            <a href="view-report-list.php" class="btn btn-info margin-bottom"><i class="fa fa-eye"></i> View all Report</a>
              <div class="row">
                  <div class="col-md-12">
                      <fieldset>
                        <legend>Report Information:</legend>
                        <form action="update-report.php" method="post" enctype="multipart/form-data" class="form-horizontal" >

                        	<div class="form-group">
                                <label class="control-label col-sm-3" >Id:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="id" value="<?php echo $reports['id']; ?>" placeholder="Id" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Patient Id:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="patient_id" value="<?php echo $reports['patients_id']; ?>" placeholder="Patient Id" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Patient Name:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="patient_name" value="<?php echo $reports['patients_name']; ?>" placeholder="Patient Name" type="text" required="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-3" >Report Title:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="report_title" value="<?php echo $reports['report_titile']; ?>" placeholder="Report Title" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                
                                <label class="col-sm-3 control-label">Upload File:</label>
                                <div class="col-sm-9">
                                    <img width="100px" style="margin-bottom: 15px;" src="images/<?php echo $reports["report_file"]; ?>"/>
                                    <input type="file" name="report_pic" value="<?php echo $reports['report_file']; ?>">
                                </div>
                                
                            </div>

                            <div class="form-group" style="margin-top:20px">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                        </form>
                     </fieldset>
                  </div>
              </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
