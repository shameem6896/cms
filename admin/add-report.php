<?php include 'header.php'; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'navigation.php'; ?>

        <div id="page-wrapper">
            <div class="container-fluid" style="margin-top: 20px;">
            <a href="view-report-list.php" class="btn btn-info margin-bottom"><i class="fa fa-eye"></i> View all Report</a>
              <div class="row">
                  <div class="col-md-12">
                      <fieldset>
                        <legend>Report Information:</legend>
                        <form action="store-report.php" method="post" enctype="multipart/form-data" class="form-horizontal" >
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Patient Id:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="patient_id" value="" placeholder="Patient Id" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Patient Name:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="patient_name" value="" placeholder="Patient Name" type="text" required="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-3" >Report Title:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="report_title" value="" placeholder="Report Title" type="text" required="">
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-sm-3">Upload Report File:</label>
                                <div class="col-sm-9">
                                    <input type="file" name="report_pic" class="form-control"></input>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top:20px">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                        </form>
                     </fieldset>
                  </div>
              </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
