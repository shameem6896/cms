<?php include 'header.php'; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'navigation.php'; ?>

        <div id="page-wrapper">
            <div class="container-fluid" style="margin-top: 20px;">
            <a href="view-doctor-list.php" class="btn btn-info margin-bottom"><i class="fa fa-eye"></i> View all Doctor</a>
              <div class="row">
                  <div class="col-md-12">
                      <fieldset>
                        <legend>Doctors Information:</legend>
                        <form action="store-doctor.php" method="post" enctype="multipart/form-data" class="form-horizontal" >
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Name:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_name" value="" placeholder="Name" type="text" required="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-3" >Specialization:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_special" value="" placeholder="Specialization" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Department:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_department" value="" placeholder="Department" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Available Date:</label>
                                <div class="col-sm-9">
                                    <input class="form-control"  name="user_available_date" value="" placeholder="Select Date" type="Date" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Choose Time:</label>
                                <div class="col-sm-9">
                                    <label class="checkbox-inline"><input type="checkbox" name="ctime" value="10am-11am">10am-11am</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="ctime" value="11am-12pm">11am-12pm</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="ctime" value="3pm-5pm">3pm-5pm</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Doctor Office:</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="doc_office">
                                        <option value="202">202</option>
                                        <option value="203">203</option>
                                        <option value="204">204</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Doctor Biodata:</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="biodata" id="description" required ></textarea>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-sm-3">Upload File:</label>
                                <div class="col-sm-9">
                                    <input type="file" name="user_pic" class="form-control"></input>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top:20px">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                        </form>
                     </fieldset>
                  </div>
              </div>
            </div>
        </div>
    </div>

<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/editor.js"></script>

<?php include 'footer.php'; ?>
