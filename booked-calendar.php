<?php include 'header.php';  ?>

    <body>

        <div id="layout">

            <header>
                <div class="container">
                    <div class="row">
                        <!--Logo-->
                        <div class="logo inner-logo">
                            <a href="index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
                        </div>
                        <!--Logo-->

                        <!--Header tools-->
                        <div class="tools-top">
                            <!--Avatar-->
                            <div class="avatar-profile">
                                <div class="user-edit">
                                    <h4>Jalal Ahamad</h4>
                                    <a href="my-account.php">edit profile</a>
                                </div>
                                <div class="avatar-image">
                                    <img src="images/avatar-profile.jpg" alt="avatar profile" class="img-responsive">
                                    <a href="appointments-reserved.php" title="2 Notifications Pending">
                                        <span class="notifications" style="display: inline;">2</span>
                                    </a>
                                </div>
                            </div>
                            <!--Avatar-->

                            <ul class="tools-help">
                                <li><a href="help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
                                <li><a href="login.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
                            </ul>
                        </div>
                        <!--Header tools-->
                    </div>
                </div>
            </header>

            <!--Menu-->
            <nav>
                <div class="container">
                    <h4 class="navbar-brand">menu</h4>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav">
                            <li> <a href="index.php">Home</a> </li>
                            <li> <a href="appointments-reserved.php">appointments reserved</a> </li>
                            <li class="active"> <a href="booked-calendar.php">book an appointment</a> </li>
                            <li> <a href="examinations.php">Result Examinations</a> </li>
                            <li> <a href="my-account.php">my account</a> </li>
                        </ul>
                       
                    </div>
                </div>
            </nav>
            <!--Menu-->

            <div class="main-container">
                <div class="container">
                    <div class="row">

                        <h3>Select Department and a Doctor then, You could add your appointment's date</h3>
                        <form class="make-app">
                            <div class="search-appointment">
                                <!--Departments-->
                                <div class="icon-data">
                                    <i class="fa fa-hospital-o"></i>*
                                </div>
                                <select>
                                    <option value="-1" selected="">Departments</option>
                                    <option value="1">Health Care</option>
                                    <option value="2">Cardic Clinic</option>
                                    <option value="3">General Surgery</option>
                                    <option value="4">Psychology</option>
                                    <option value="5">Pediatrics</option>
                                </select>
                                <!--Departments-->

                                <!--name-->
                                <div class="icon-data">
                                    <i class="fa fa-user-md"></i>*
                                </div>
                                <select>
                                    <option value="-1" selected="">Doctors</option>
                                    <option value="1">Prof. (Dr.) Asia Khanam</option>
                                    <option value="2">Prof. Dr. Abu Nasar Rizvi</option>
                                    <option value="3">Dr. Amjad Hossain</option>
                                    <option value="4">DR. MD. MOFIZ UDDIN</option>
                                    <option value="5">Dr. Md. Lokman Hossain</option>
                                </select>
                                <!--name-->
                                 <!--date-->
                                <div class="icon-data">
                                    <i class="fa fa-calendar-plus-o"></i>*
                                </div>
                                <input type="date" name="date" placeholder="select date">
                                <!--date-->
                                <!--time-->
                                <div class="icon-data">
                                    <i class="fa fa-clock-o"></i>*
                                </div>
                                <select>
                                    <option value="-1" selected="">Select Time</option>
                                    <option value="1">10am-11am</option>
                                    <option value="2">11am-12pm</option>
                                    <option value="3">12pm-1pm</option>
                                    <option value="4">3pm-3.30pm</option>
                                    <option value="5">5pm-6pm</option>
                                </select>
                                <!--time-->
                                <!--time-->
                                <div class="icon-data">
                                    <i class="fa fa-check-square-o"></i>*
                                </div>
                                <select>
                                    <option value="-1" selected="">Type of Appointment</option>
                                    <option value="1">General</option>
                                    <option value="2">Specialist</option>
                                </select>
                                <!--time-->

                                <span class="btn btn-green btn-small btn-search-appointment">
                                    <i class="fa fa-search"></i> 
                                    <a href="appointments-reserved.php">Book</a>
                                </span>
                            </div>
                         </form>
                        <div class="preview-appointment">
                            <div class="preview-doctor">
                                <img src="images/doctor11.jpg" alt="doctor preview" class="img-responsive">
                            </div>
                            <ul>
                                <li><h5>Prof. (Dr.) Asia Khanam</h5><a href="meet-doctors.php">more about</a></li>
                                <li>Specialization: <span>MBBS, MD(Nephrology)</span></li>
                                <li>Price Appointment: <span class="price">1500</span></li>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
         </div>   
        <?php include 'footer.php';  ?>
