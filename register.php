<?php include 'header.php';  ?>

    <body class="panel-access">

        <div id="layout">
             <!--Login-->
                <div class="login">
                    <div class="container">
                        <div class="register-form">

                            <!--Data form-->
                            <div class="data-form">
                                <span class="back-to-login">
                                    <a class="btn btn-green btn-xsmall" href="login.php"><i class="fa fa-angle-double-left"></i> Back to Login</a>
                                </span>
                                <span class="back-to-homepage">
                                    <a class="btn btn-green btn-xsmall" href="index.php"><i class="fa fa-home"></i> Back to Homepage</a>
                                </span>
                                <!--Logo-->
                                <a href="register.php" class="logo reg-logo"><img src="images/login-logo.png" alt="logo"></a>
                                <!--Logo-->

                                <!--Form-->
                                <div class="form-login">
                                    <form action="admin/store-patient.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <!--Personal Information-->
                                        <div class="row">

                                            <h3>Personal Information</h3>
                                            <div class="datapos">

                                                <!--name-->
                                                <div class="icon-data">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="text" name="user_fname" placeholder="First Name">
                                                <!--name-->

                                                 <!--email-->
                                                <div class="icon-data">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                                <input type="email" name="user_email" placeholder="Your Email">
                                                <!--email-->
                                               
                                                <!--Alternative phone-->
                                                <div class="icon-data">
                                                    <i class="fa fa-user-circle"></i>
                                                </div>
                                                <input type="password"  name="user_pass" placeholder="Password">
                                                <!--Alternative phone-->
                                                <!--phone-->
                                                <div class="icon-data">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input type="number" name="user_num" placeholder="Your Phone">
                                                <!--genere-->
                                                <label>
                                                    <input type="radio" name="inlineRadioOptions" value="female"> <span>Female</span>
                                                </label>
                                                <label>
                                                    <input type="radio" name="inlineRadioOptions" value="male"> Male
                                                </label>
                                                <!--genere-->
                                            </div>

                                            <div class="datapos">
                                            <!--Last name-->
                                                <div class="icon-data">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="text" name="user_lname" placeholder="Last name">
                                                <!--Last name-->

                                                   <!--Last name-->
                                                <div class="icon-data">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="text" name="user_name" placeholder="Username">
                                                <!--Last name-->

                                                 <!--number document-->
                                                <div class="icon-data">
                                                    <i class="fa fa-user-circle"></i>
                                                </div>
                                                <input type="password" name="user_pass" placeholder="Confirm Password">
                                                <!--number document-->
                                                <!--Avatar Profile-->
                                                <div class="avatar-profile">
                                                    <label>Image Profile</label>
                                                    <input type="file" name="user_pic" >
                                                </div>
                                                <!--Avatar Profile-->
                                              
                                            </div>

                                        </div>
                                        <!--Personal Information-->

                                        <button type="submit"  name="submit" value="submit" class="btn btn-default">Create Account</button>

                                        <span class="help">
                                            <a href="help.php" class="help-link">Help?</a>
                                        </span>
                                    </form>
                                </div>
                                <!--Form-->
                            </div>
                            <!--Data form-->
                        </div>
                    </div>
                </div>
                <!--Login-->

    <?php include 'footer.php';  ?>
    